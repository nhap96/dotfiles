#!/bin/sh

# requires https://github.com/PandorasFox/i3lock-color
# additional arguments are also passed to i3lock
i3lock --blur=10\
       --indicator\
       --clock\
       --datestr="%a, %b %e"\
       --timestr='%H:%M'\
       --datesize=20\
       --timesize=37\
       --timecolor='#66b5ffff'\
       --datecolor='#66b5ffaa'\
       $@
