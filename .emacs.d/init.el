;; Auto start server if it's not running already
;; https://wiki.archlinux.org/index.php/Emacs#Multiplexing_emacs_and_emacsclient
(require 'server)
(unless (server-running-p)
  (server-start))

;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.
(package-initialize)

(setq custom-file (expand-file-name "custom.el" user-emacs-directory))
(when (file-exists-p custom-file)
  (load custom-file))

;; https://melpa.org/#/getting-started
(require 'package)
(let* ((no-ssl (and (memq system-type '(windows-nt ms-dos))
                    (not (gnutls-available-p))))
       (proto (if no-ssl "http" "https")))
  ;; Comment/uncomment these two lines to enable/disable MELPA and MELPA Stable as desired
  (add-to-list 'package-archives (cons "melpa" (concat proto "://melpa.org/packages/")) t)
  ;;(add-to-list 'package-archives (cons "melpa-stable" (concat proto "://stable.melpa.org/packages/")) t)
  (when (< emacs-major-version 24)
    ;; For important compatibility libraries like cl-lib
    (add-to-list 'package-archives '("gnu" . (concat proto "://elpa.gnu.org/packages/")))))

;; Set default font
(set-face-attribute 'default nil
                    :family "Source Code Pro for Powerline"
                    :height 105
                    ;; :family "Inconsolata for Powerline"
                    ;; :height 120
                    :weight 'normal
                    :width 'normal)

;; Better list-buffers
(global-set-key "\C-x\C-b" 'ibuffer)

;; Disable ^Z
;; https://stackoverflow.com/a/28205749/5842888
(global-unset-key (kbd "C-z"))
(global-set-key (kbd "C-z C-z") 'my-suspend-frame)
(defun my-suspend-frame ()
  "In a GUI environment, do nothing; otherwise `suspend-frame'."
  (interactive)
  (if (display-graphic-p)
      (message "suspend-frame disabled for graphical displays.")
    (suspend-frame)))

;; Not to split window at startup
;; https://emacs.stackexchange.com/a/5875
(setq inhibit-startup-screen t)

;; Prompt for confirmation when leaving Emacs
(setq confirm-kill-emacs 'y-or-n-p)

;; Display column number in mode line
(setq column-number-mode t)

;; Spaces {{{
;; Highlight trailing whitespace
(setq-default show-trailing-whitespace t)
;; Show TABs Using a String (^I)
;; Highlight TABs
(progn
  (defface extra-whitespace-face
    '((t (:background "pale green")))
    "Used for tabs and such.")
  (defvar my-extra-keywords
    '(("\t" . 'extra-whitespace-face)))
  (add-hook 'emacs-lisp-mode-hook
            (lambda () (font-lock-add-keywords nil my-extra-keywords)))
  (add-hook 'text-mode-hook
            (lambda () (font-lock-add-keywords nil my-extra-keywords))))
;; Use spaces intead of tabs when indenting
(setq-default indent-tabs-mode nil)
;; }}}

;; Disable tool bar
(tool-bar-mode -1)
;; Disable scroll bar. Note that this has to be turned of before menu bar is disabled
(toggle-scroll-bar -1)
;; Disable menu bar
(menu-bar-mode -1)

;; Show Paren mode
(show-paren-mode 1)

;; Electric Pair mode
(electric-pair-mode 1)

;; Global flycheck mode
(global-flycheck-mode)

;; Prettify-Symbols mode {{{
(global-prettify-symbols-mode)
(setq prettify-symbols-unprettify-at-point t)
;; }}}

;; https://www.emacswiki.org/emacs/RecentFiles
(recentf-mode 1)
(setq recentf-max-menu-items 100
      recentf-max-saved-items 1000)

(setq backup-by-copying t ; don't clobber symlinks
      backup-directory-alist
      '(("." . "~/.emacs.d/saves/")) ; don't litter my fs tree
      delete-old-versions t
      kept-new-versions 6
      kept-old-versions 2
      version-control t) ; use versioned backups

(require 'dired-x)

(read-abbrev-file "~/.emacs.d/abbrev_defs.el")

;; ----- {{{
(add-to-list 'load-path "~/.emacs.d/elisp/")
(load-library "cdlatex-el")
(load-library "cfparser")
;; }}}

(defun insert-current-date-time ()
  "Insert current date & time, e.g. Fri 28 Dec 2018 08:23:24 AM EST."
  (interactive)
  (insert (shell-command-to-string "echo -n $(date '+%c')")))

;; =========================== use-package ===========================

;; use-package {{{
;; This is only needed once, near the top of the file
(eval-when-compile
  (require 'use-package))
(setq use-package-verbose t)
(setq use-package-always-ensure t)
;; }}}

;; Builtin {{{
(use-package org
  :config
  (progn
    ;; display header indentation
    (add-hook 'org-mode-hook 'org-indent-mode)
    ;; wrap lines
    (add-hook 'org-mode-hook 'visual-line-mode)
    ))

(use-package eldoc
  :hook
  ((emacs-lisp-mode
    lisp-interaction-mode
    ielm-mode)
   . eldoc-mode))
;; }}}

;; Theme {{{
;; (use-package dracula-theme
;;   :config
;;   (load-theme 'dracula t)
;;   ;; dracula mode-line-inactive background conflicts with text background
;;   (copy-face 'mode-line 'mode-line-inactive)
;;   (set-face-background 'mode-line "dim grey")
;;   )
(use-package monokai-theme
  :config
  (load-theme 'monokai t)
  )
;; }}}

(use-package yasnippet
  :config
  (progn
    (yas-global-mode 1)
    ))

(use-package helm
  :config
  (progn
    (helm-mode 1)

    ;; Recommended commands from https://github.com/emacs-helm/helm/wiki#quick-try {{{
    (define-key global-map [remap find-file] 'helm-find-files)
    (define-key global-map [remap occur] 'helm-occur)
    ;; (define-key global-map [remap list-buffers] 'helm-buffers-list)
    (define-key global-map [remap dabbrev-expand] 'helm-dabbrev)
    (define-key global-map [remap execute-extended-command] 'helm-M-x)
    (unless (boundp 'completion-in-region-function)
      (define-key lisp-interaction-mode-map [remap completion-at-point] 'helm-lisp-completion-at-point)
      (define-key emacs-lisp-mode-map       [remap completion-at-point] 'helm-lisp-completion-at-point))
    ;; }}}

    (setq helm-mode-fuzzy-match t)
    (setq helm-completion-in-region-fuzzy-match t)
    (setq helm-M-x-fuzzy-match t)
    ;; Fix [Display not ready] when hitting enter too fast. https://github.com/emacs-helm/helm/issues/550
    (setq helm-exit-idle-delay 0)

    (global-set-key "\C-x\ \C-r" 'helm-recentf)
    ))

(use-package company
  :config
  (progn
    ;; Integration with company-mode
    (add-hook 'after-init-hook 'global-company-mode)
    (setq company-minimum-prefix-length 1)
    ;; Speed up company-mode
    ;; https://emacs.stackexchange.com/a/23937
    (setq company-dabbrev-downcase 0)
    (setq company-idle-delay 0) ;; This is believed to be power-comsuming
    ;; Disable TAB usage
    (define-key company-active-map [tab] nil)
    (define-key company-active-map (kbd "TAB") nil)
    ))

(use-package irony
  :hook
  ((c++-mode c-mode)
   . irony-mode)
  :config
  (progn
    (add-hook 'irony-mode-hook 'irony-cdb-autosetup-compile-options)
    (use-package flycheck-irony
      :config
      (progn
        (add-hook 'flycheck-mode-hook #'flycheck-irony-setup)
        ))
    (use-package company-irony
      :requires company
      :config
      (progn
        (add-to-list 'company-backends 'company-irony)
        ))
    ))

(use-package evil
  :init
  (progn
    ;; Don't expand abbrevs when exit insert mode
    ;; https://github.com/syl20bnr/spacemacs/issues/8853#issuecomment-302706114
    (setq evil-want-abbrev-expand-on-insert-exit nil)
    (setq evil-want-C-u-scroll t)
    )
  :config
  (progn
    (evil-mode 1)
    ;; Disable visual-mode automatically copies selected text
    ;; https://stackoverflow.com/a/38286385
    (fset 'evil-visual-update-x-selection 'ignore)
    ;; Treat _ as part of the word (like vim)
    ;; https://emacs.stackexchange.com/a/20717/18034
    (defalias #'forward-evil-word #'forward-evil-symbol)
    ;; https://stackoverflow.com/a/20899418/5842888
    (progn
      ;; Make movement keys work like they should
      (define-key evil-normal-state-map (kbd "<remap> <evil-next-line>") 'evil-next-visual-line)
      (define-key evil-normal-state-map (kbd "<remap> <evil-previous-line>") 'evil-previous-visual-line)
      (define-key evil-motion-state-map (kbd "<remap> <evil-next-line>") 'evil-next-visual-line)
      (define-key evil-motion-state-map (kbd "<remap> <evil-previous-line>") 'evil-previous-visual-line)
      ;; Make horizontal movement cross lines
      ;; (setq-default evil-cross-lines t)
      )
    (use-package evil-surround
      :config
      (progn
      (global-evil-surround-mode 1)
      ))
    ))

;; When AUCTeX is installed, latex-mode (which is from TeX package) and LaTeX-mode
;; are actually the same. https://stackoverflow.com/a/17778046/5842888
(use-package tex
  :defer t
  ;; https://github.com/jwiegley/use-package/issues/379#issuecomment-246161500
  :ensure auctex
  :after latex
  :config
  (let ((hooks '(LaTeX-mode-hook latex-mode-hook))
        (maps '(LaTeX-mode-map latex-mode-map)))
    (dolist (hook hooks)
      (add-hook hook #'abbrev-mode)
      ;; (add-hook hook #'TeX-fold-mode) ;; conflicts with outline-minor-mode-prefix
      (add-hook hook #'outline-minor-mode)
      (add-hook hook #'reftex-mode)
      (add-hook hook #'latex-math-mode)
      ;; Synctex
      (add-hook hook #'TeX-source-correlate-mode)
      )
    ;; Don't ask for saving when compile
    (setq TeX-save-query nil)
    ;; outline {{{
    (setq outline-minor-mode-prefix "\C-c\C-o")
    ;; }}}
    ;; preview-latex {{{
    (setq preview-image-type 'dvipng) ;; requires 'dvipng' installed
    (setq preview-auto-cache-preamble t)
    (setq preview-scale-function 1.25)
    ;; }}}
    ;; C-c C-a to compile directly
    ;; https://www.reddit.com/r/emacs/comments/3o8wfy/auctex_add_compile_shortcut/
    (progn
      (defun latex-compile ()
        (interactive)
        ;; (save-buffer)
        (TeX-command "LaTeX" 'TeX-master-file))
      (dolist (map maps)
        (define-key (symbol-value map) (kbd "C-c C-c") 'latex-compile)))
    (add-to-list 'TeX-view-program-selection '(output-pdf "Zathura"))
    ))

(use-package helpful
  :config
  (progn
    ;; SOME of recommended keybindings {{{
    ;; Note that the built-in `describe-function' includes both functions
    ;; and macros. `helpful-function' is functions only, so we provide
    ;; `helpful-callable' as a drop-in replacement.
    (global-set-key (kbd "C-h f") #'helpful-callable)

    (global-set-key (kbd "C-h v") #'helpful-variable)
    (global-set-key (kbd "C-h k") #'helpful-key)

    ;; Look up *F*unctions (excludes macros).
    ;;
    ;; By default, C-h F is bound to `Info-goto-emacs-command-node'. Helpful
    ;; already links to the manual, if a function is referenced there.
    (global-set-key (kbd "C-h F") #'helpful-function)
    ;; }}}
    ))

(use-package elisp-demos
  :requires helpful
  :config
  (progn
    (advice-add 'helpful-update :after #'elisp-demos-advice-helpful-update)
    ))

(use-package undo-tree
  :config
  (progn
    (global-undo-tree-mode)
    ))

;;; Temporary disable for now since couldn't get to load it lazily
;; (use-package proof-general
;;   ;; https://github.com/ProofGeneral/PG/issues/398
;;   :no-require t
;;   :config
;;   (progn
;;     (use-package company-coq
;;       :requires company
;;       :config
;;       (progn
;;         (add-hook 'coq-mode-hook #'company-coq-mode)
;;         ))
;;     ))

(use-package rainbow-delimiters
  :config
  (progn
    (add-hook 'prog-mode-hook #'rainbow-delimiters-mode)
    ))

(use-package paredit
  :hook ((emacs-lisp-mode
          eval-expression-minibuffer-setup
          ielm-mode
          lisp-mode
          lisp-interaction-mode
          scheme-mode)
         . enable-paredit-mode)
  :config
  (progn
    ;; Integration with eldoc
    ;; https://www.emacswiki.org/emacs/ParEdit#toc2
    (progn
      (require 'eldoc) ; if not already loaded
      (eldoc-add-command
       'paredit-backward-delete
       'paredit-close-round)
      )
    ))
